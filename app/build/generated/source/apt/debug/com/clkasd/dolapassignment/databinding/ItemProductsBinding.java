package com.clkasd.dolapassignment.databinding;
import com.clkasd.dolapassignment.R;
import com.clkasd.dolapassignment.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemProductsBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    public final android.widget.LinearLayout itemProductLinlay;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.TextView mboundView3;
    @NonNull
    public final android.widget.ImageView productImage;
    // variables
    @Nullable
    private com.clkasd.dolapassignment.viewModel.ProductsItemViewModel mItemProductViewModel;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemProductsBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 1);
        final Object[] bindings = mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds);
        this.itemProductLinlay = (android.widget.LinearLayout) bindings[0];
        this.itemProductLinlay.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.TextView) bindings[3];
        this.mboundView3.setTag(null);
        this.productImage = (android.widget.ImageView) bindings[1];
        this.productImage.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemProductViewModel == variableId) {
            setItemProductViewModel((com.clkasd.dolapassignment.viewModel.ProductsItemViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemProductViewModel(@Nullable com.clkasd.dolapassignment.viewModel.ProductsItemViewModel ItemProductViewModel) {
        updateRegistration(0, ItemProductViewModel);
        this.mItemProductViewModel = ItemProductViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemProductViewModel);
        super.requestRebind();
    }
    @Nullable
    public com.clkasd.dolapassignment.viewModel.ProductsItemViewModel getItemProductViewModel() {
        return mItemProductViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemProductViewModel((com.clkasd.dolapassignment.viewModel.ProductsItemViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemProductViewModel(com.clkasd.dolapassignment.viewModel.ProductsItemViewModel ItemProductViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String itemProductViewModelOriginalPrice = null;
        java.lang.String itemProductViewModelStrikeThrough = null;
        com.clkasd.dolapassignment.viewModel.ProductsItemViewModel itemProductViewModel = mItemProductViewModel;
        java.lang.String itemProductViewModelProductImage = null;
        java.lang.String itemProductViewModelPrice = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (itemProductViewModel != null) {
                    // read itemProductViewModel.OriginalPrice
                    itemProductViewModelOriginalPrice = itemProductViewModel.getOriginalPrice();
                    // read itemProductViewModel.strikeThrough
                    itemProductViewModelStrikeThrough = itemProductViewModel.getStrikeThrough();
                    // read itemProductViewModel.ProductImage
                    itemProductViewModelProductImage = itemProductViewModel.getProductImage();
                    // read itemProductViewModel.Price
                    itemProductViewModelPrice = itemProductViewModel.getPrice();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel.strikeThrough(this.mboundView2, itemProductViewModelStrikeThrough);
            this.mboundView2.setText(itemProductViewModelOriginalPrice);
            this.mboundView3.setText(itemProductViewModelPrice);
            com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel.setImageUrl(this.productImage, itemProductViewModelProductImage);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ItemProductsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemProductsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ItemProductsBinding>inflate(inflater, com.clkasd.dolapassignment.R.layout.item_products, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ItemProductsBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemProductsBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.clkasd.dolapassignment.R.layout.item_products, null, false), bindingComponent);
    }
    @NonNull
    public static ItemProductsBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemProductsBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/item_products_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ItemProductsBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): itemProductViewModel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}