package com.clkasd.dolapassignment.databinding;
import com.clkasd.dolapassignment.R;
import com.clkasd.dolapassignment.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemComponentBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    public final android.widget.ImageView banner;
    @NonNull
    public final android.support.v7.widget.RecyclerView componentsRecycler;
    @NonNull
    public final android.widget.LinearLayout itemComponentRellay;
    @NonNull
    private final android.widget.TextView mboundView1;
    @NonNull
    public final android.widget.ImageView singleBanner;
    // variables
    @Nullable
    private com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel mItemComponentViewModel;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemComponentBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 6);
        final Object[] bindings = mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds);
        this.banner = (android.widget.ImageView) bindings[3];
        this.banner.setTag(null);
        this.componentsRecycler = (android.support.v7.widget.RecyclerView) bindings[4];
        this.componentsRecycler.setTag(null);
        this.itemComponentRellay = (android.widget.LinearLayout) bindings[0];
        this.itemComponentRellay.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        this.singleBanner = (android.widget.ImageView) bindings[2];
        this.singleBanner.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemComponentViewModel == variableId) {
            setItemComponentViewModel((com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemComponentViewModel(@Nullable com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel ItemComponentViewModel) {
        updateRegistration(1, ItemComponentViewModel);
        this.mItemComponentViewModel = ItemComponentViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.itemComponentViewModel);
        super.requestRebind();
    }
    @Nullable
    public com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel getItemComponentViewModel() {
        return mItemComponentViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemComponentViewModelSingleBannerVisibility((android.databinding.ObservableInt) object, fieldId);
            case 1 :
                return onChangeItemComponentViewModel((com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel) object, fieldId);
            case 2 :
                return onChangeItemComponentViewModelHeaderTextVisibility((android.databinding.ObservableInt) object, fieldId);
            case 3 :
                return onChangeItemComponentViewModelHeaderText((android.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeItemComponentViewModelRecyclerVisibility((android.databinding.ObservableInt) object, fieldId);
            case 5 :
                return onChangeItemComponentViewModelBannerVisibility((android.databinding.ObservableInt) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemComponentViewModelSingleBannerVisibility(android.databinding.ObservableInt ItemComponentViewModelSingleBannerVisibility, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemComponentViewModel(com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel ItemComponentViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemComponentViewModelHeaderTextVisibility(android.databinding.ObservableInt ItemComponentViewModelHeaderTextVisibility, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemComponentViewModelHeaderText(android.databinding.ObservableField<java.lang.String> ItemComponentViewModelHeaderText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemComponentViewModelRecyclerVisibility(android.databinding.ObservableInt ItemComponentViewModelRecyclerVisibility, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeItemComponentViewModelBannerVisibility(android.databinding.ObservableInt ItemComponentViewModelBannerVisibility, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.databinding.ObservableInt itemComponentViewModelSingleBannerVisibility = null;
        java.lang.String itemComponentViewModelSingleBannerPicture = null;
        com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel itemComponentViewModel = mItemComponentViewModel;
        int itemComponentViewModelSingleBannerVisibilityGet = 0;
        android.databinding.ObservableInt itemComponentViewModelHeaderTextVisibility = null;
        java.util.List<com.clkasd.dolapassignment.model.Product> itemComponentViewModelProductList = null;
        int itemComponentViewModelHeaderTextVisibilityGet = 0;
        int itemComponentViewModelBannerVisibilityGet = 0;
        android.databinding.ObservableField<java.lang.String> itemComponentViewModelHeaderText = null;
        android.databinding.ObservableInt itemComponentViewModelRecyclerVisibility = null;
        android.databinding.ObservableInt itemComponentViewModelBannerVisibility = null;
        java.lang.String itemComponentViewModelHeaderTextGet = null;
        int itemComponentViewModelRecyclerVisibilityGet = 0;

        if ((dirtyFlags & 0x7fL) != 0) {


            if ((dirtyFlags & 0x43L) != 0) {

                    if (itemComponentViewModel != null) {
                        // read itemComponentViewModel.singleBannerVisibility
                        itemComponentViewModelSingleBannerVisibility = itemComponentViewModel.singleBannerVisibility;
                    }
                    updateRegistration(0, itemComponentViewModelSingleBannerVisibility);


                    if (itemComponentViewModelSingleBannerVisibility != null) {
                        // read itemComponentViewModel.singleBannerVisibility.get()
                        itemComponentViewModelSingleBannerVisibilityGet = itemComponentViewModelSingleBannerVisibility.get();
                    }
            }
            if ((dirtyFlags & 0x42L) != 0) {

                    if (itemComponentViewModel != null) {
                        // read itemComponentViewModel.SingleBannerPicture
                        itemComponentViewModelSingleBannerPicture = itemComponentViewModel.getSingleBannerPicture();
                        // read itemComponentViewModel.ProductList
                        itemComponentViewModelProductList = itemComponentViewModel.getProductList();
                    }
            }
            if ((dirtyFlags & 0x46L) != 0) {

                    if (itemComponentViewModel != null) {
                        // read itemComponentViewModel.headerTextVisibility
                        itemComponentViewModelHeaderTextVisibility = itemComponentViewModel.headerTextVisibility;
                    }
                    updateRegistration(2, itemComponentViewModelHeaderTextVisibility);


                    if (itemComponentViewModelHeaderTextVisibility != null) {
                        // read itemComponentViewModel.headerTextVisibility.get()
                        itemComponentViewModelHeaderTextVisibilityGet = itemComponentViewModelHeaderTextVisibility.get();
                    }
            }
            if ((dirtyFlags & 0x4aL) != 0) {

                    if (itemComponentViewModel != null) {
                        // read itemComponentViewModel.headerText
                        itemComponentViewModelHeaderText = itemComponentViewModel.headerText;
                    }
                    updateRegistration(3, itemComponentViewModelHeaderText);


                    if (itemComponentViewModelHeaderText != null) {
                        // read itemComponentViewModel.headerText.get()
                        itemComponentViewModelHeaderTextGet = itemComponentViewModelHeaderText.get();
                    }
            }
            if ((dirtyFlags & 0x52L) != 0) {

                    if (itemComponentViewModel != null) {
                        // read itemComponentViewModel.recyclerVisibility
                        itemComponentViewModelRecyclerVisibility = itemComponentViewModel.recyclerVisibility;
                    }
                    updateRegistration(4, itemComponentViewModelRecyclerVisibility);


                    if (itemComponentViewModelRecyclerVisibility != null) {
                        // read itemComponentViewModel.recyclerVisibility.get()
                        itemComponentViewModelRecyclerVisibilityGet = itemComponentViewModelRecyclerVisibility.get();
                    }
            }
            if ((dirtyFlags & 0x62L) != 0) {

                    if (itemComponentViewModel != null) {
                        // read itemComponentViewModel.bannerVisibility
                        itemComponentViewModelBannerVisibility = itemComponentViewModel.bannerVisibility;
                    }
                    updateRegistration(5, itemComponentViewModelBannerVisibility);


                    if (itemComponentViewModelBannerVisibility != null) {
                        // read itemComponentViewModel.bannerVisibility.get()
                        itemComponentViewModelBannerVisibilityGet = itemComponentViewModelBannerVisibility.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x62L) != 0) {
            // api target 1

            this.banner.setVisibility(itemComponentViewModelBannerVisibilityGet);
        }
        if ((dirtyFlags & 0x42L) != 0) {
            // api target 1

            com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel.setImageUrl(this.banner, itemComponentViewModelSingleBannerPicture);
            com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel.setSource(this.componentsRecycler, itemComponentViewModelProductList);
            com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel.setImageUrl(this.singleBanner, itemComponentViewModelSingleBannerPicture);
        }
        if ((dirtyFlags & 0x52L) != 0) {
            // api target 1

            this.componentsRecycler.setVisibility(itemComponentViewModelRecyclerVisibilityGet);
        }
        if ((dirtyFlags & 0x46L) != 0) {
            // api target 1

            this.mboundView1.setVisibility(itemComponentViewModelHeaderTextVisibilityGet);
        }
        if ((dirtyFlags & 0x4aL) != 0) {
            // api target 1

            this.mboundView1.setText(itemComponentViewModelHeaderTextGet);
        }
        if ((dirtyFlags & 0x43L) != 0) {
            // api target 1

            this.singleBanner.setVisibility(itemComponentViewModelSingleBannerVisibilityGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ItemComponentBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemComponentBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ItemComponentBinding>inflate(inflater, com.clkasd.dolapassignment.R.layout.item_component, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ItemComponentBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemComponentBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.clkasd.dolapassignment.R.layout.item_component, null, false), bindingComponent);
    }
    @NonNull
    public static ItemComponentBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ItemComponentBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/item_component_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ItemComponentBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): itemComponentViewModel.singleBannerVisibility
        flag 1 (0x2L): itemComponentViewModel
        flag 2 (0x3L): itemComponentViewModel.headerTextVisibility
        flag 3 (0x4L): itemComponentViewModel.headerText
        flag 4 (0x5L): itemComponentViewModel.recyclerVisibility
        flag 5 (0x6L): itemComponentViewModel.bannerVisibility
        flag 6 (0x7L): null
    flag mapping end*/
    //end
}