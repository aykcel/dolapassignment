package com.clkasd.dolapassignment.databinding;
import com.clkasd.dolapassignment.R;
import com.clkasd.dolapassignment.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityDashboardBinding extends android.databinding.ViewDataBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar, 4);
    }
    // views
    @NonNull
    public final android.widget.TextView labelStatus;
    @NonNull
    public final android.support.v7.widget.RecyclerView listComponents;
    @NonNull
    private final android.support.design.widget.CoordinatorLayout mboundView0;
    @NonNull
    public final android.widget.ProgressBar progressComponents;
    @NonNull
    public final android.support.v7.widget.Toolbar toolbar;
    // variables
    @Nullable
    private com.clkasd.dolapassignment.viewModel.DashboardViewModel mMainViewModel;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityDashboardBinding(@NonNull android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        super(bindingComponent, root, 4);
        final Object[] bindings = mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds);
        this.labelStatus = (android.widget.TextView) bindings[2];
        this.labelStatus.setTag(null);
        this.listComponents = (android.support.v7.widget.RecyclerView) bindings[3];
        this.listComponents.setTag(null);
        this.mboundView0 = (android.support.design.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.progressComponents = (android.widget.ProgressBar) bindings[1];
        this.progressComponents.setTag(null);
        this.toolbar = (android.support.v7.widget.Toolbar) bindings[4];
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.mainViewModel == variableId) {
            setMainViewModel((com.clkasd.dolapassignment.viewModel.DashboardViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMainViewModel(@Nullable com.clkasd.dolapassignment.viewModel.DashboardViewModel MainViewModel) {
        this.mMainViewModel = MainViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.mainViewModel);
        super.requestRebind();
    }
    @Nullable
    public com.clkasd.dolapassignment.viewModel.DashboardViewModel getMainViewModel() {
        return mMainViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeMainViewModelDolapLabel((android.databinding.ObservableInt) object, fieldId);
            case 1 :
                return onChangeMainViewModelDolapRecycler((android.databinding.ObservableInt) object, fieldId);
            case 2 :
                return onChangeMainViewModelMessageLabel((android.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeMainViewModelDolapProgress((android.databinding.ObservableInt) object, fieldId);
        }
        return false;
    }
    private boolean onChangeMainViewModelDolapLabel(android.databinding.ObservableInt MainViewModelDolapLabel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMainViewModelDolapRecycler(android.databinding.ObservableInt MainViewModelDolapRecycler, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMainViewModelMessageLabel(android.databinding.ObservableField<java.lang.String> MainViewModelMessageLabel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeMainViewModelDolapProgress(android.databinding.ObservableInt MainViewModelDolapProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int mainViewModelDolapLabelGet = 0;
        android.databinding.ObservableInt mainViewModelDolapLabel = null;
        int mainViewModelDolapProgressGet = 0;
        java.lang.String mainViewModelMessageLabelGet = null;
        com.clkasd.dolapassignment.viewModel.DashboardViewModel mainViewModel = mMainViewModel;
        android.databinding.ObservableInt mainViewModelDolapRecycler = null;
        int mainViewModelDolapRecyclerGet = 0;
        android.databinding.ObservableField<java.lang.String> mainViewModelMessageLabel = null;
        android.databinding.ObservableInt mainViewModelDolapProgress = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (mainViewModel != null) {
                        // read mainViewModel.dolapLabel
                        mainViewModelDolapLabel = mainViewModel.dolapLabel;
                    }
                    updateRegistration(0, mainViewModelDolapLabel);


                    if (mainViewModelDolapLabel != null) {
                        // read mainViewModel.dolapLabel.get()
                        mainViewModelDolapLabelGet = mainViewModelDolapLabel.get();
                    }
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (mainViewModel != null) {
                        // read mainViewModel.dolapRecycler
                        mainViewModelDolapRecycler = mainViewModel.dolapRecycler;
                    }
                    updateRegistration(1, mainViewModelDolapRecycler);


                    if (mainViewModelDolapRecycler != null) {
                        // read mainViewModel.dolapRecycler.get()
                        mainViewModelDolapRecyclerGet = mainViewModelDolapRecycler.get();
                    }
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (mainViewModel != null) {
                        // read mainViewModel.messageLabel
                        mainViewModelMessageLabel = mainViewModel.messageLabel;
                    }
                    updateRegistration(2, mainViewModelMessageLabel);


                    if (mainViewModelMessageLabel != null) {
                        // read mainViewModel.messageLabel.get()
                        mainViewModelMessageLabelGet = mainViewModelMessageLabel.get();
                    }
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (mainViewModel != null) {
                        // read mainViewModel.dolapProgress
                        mainViewModelDolapProgress = mainViewModel.dolapProgress;
                    }
                    updateRegistration(3, mainViewModelDolapProgress);


                    if (mainViewModelDolapProgress != null) {
                        // read mainViewModel.dolapProgress.get()
                        mainViewModelDolapProgressGet = mainViewModelDolapProgress.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.labelStatus, mainViewModelMessageLabelGet);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            this.labelStatus.setVisibility(mainViewModelDolapLabelGet);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            this.listComponents.setVisibility(mainViewModelDolapRecyclerGet);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            this.progressComponents.setVisibility(mainViewModelDolapProgressGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;

    @NonNull
    public static ActivityDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return android.databinding.DataBindingUtil.<ActivityDashboardBinding>inflate(inflater, com.clkasd.dolapassignment.R.layout.activity_dashboard, root, attachToRoot, bindingComponent);
    }
    @NonNull
    public static ActivityDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityDashboardBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return bind(inflater.inflate(com.clkasd.dolapassignment.R.layout.activity_dashboard, null, false), bindingComponent);
    }
    @NonNull
    public static ActivityDashboardBinding bind(@NonNull android.view.View view) {
        return bind(view, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static ActivityDashboardBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        if (!"layout/activity_dashboard_0".equals(view.getTag())) {
            throw new RuntimeException("view tag isn't correct on view:" + view.getTag());
        }
        return new ActivityDashboardBinding(bindingComponent, view);
    }
    /* flag mapping
        flag 0 (0x1L): mainViewModel.dolapLabel
        flag 1 (0x2L): mainViewModel.dolapRecycler
        flag 2 (0x3L): mainViewModel.messageLabel
        flag 3 (0x4L): mainViewModel.dolapProgress
        flag 4 (0x5L): mainViewModel
        flag 5 (0x6L): null
    flag mapping end*/
    //end
}