package com.clkasd.dolapassignment.viewModel;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;

import com.clkasd.dolapassignment.data.DolapResponse;
import com.clkasd.dolapassignment.data.DolapService;
import com.clkasd.dolapassignment.DolapApplication;
import com.clkasd.dolapassignment.model.Components;
import com.clkasd.dolapassignment.model.Image;
import com.clkasd.dolapassignment.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Aykut on 29.01.2018.
 */

public class DashboardViewModel extends Observable {
    public ObservableInt dolapProgress;
    public ObservableInt dolapRecycler;
    public ObservableInt dolapLabel;
    public ObservableField<String> messageLabel;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<Components> componentsList;
    private Context context;

    public DashboardViewModel(@NonNull Context context)
    {
        this.context = context;
        this.componentsList = new ArrayList<>();
        dolapProgress = new ObservableInt(View.GONE);
        dolapRecycler = new ObservableInt(View.GONE);
        dolapLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_dolap));
        init();
    }

    private void init() {
        initializeViews();
        fetchInventory();
    }
    private void initializeViews() {
        dolapLabel.set(View.GONE);
        dolapRecycler.set(View.GONE);
        dolapProgress.set(View.VISIBLE);
    }
    private void fetchInventory() {
        DolapApplication dolapApplication = DolapApplication.create(context);
        DolapService dolapService = dolapApplication.getDolapService();

        Disposable disposable = dolapService.fetchResponse(new Image())//boş gönderme amaçlı
                .subscribeOn(dolapApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<DolapResponse>() {
                    @Override public void accept(DolapResponse dolapResponse) throws Exception {
                        changeComponentsDataSet(dolapResponse.getComponentsList());
                        dolapProgress.set(View.GONE);
                        dolapLabel.set(View.GONE);
                        dolapRecycler.set(View.VISIBLE);
                    }
                }, new Consumer<Throwable>() {
                    @Override public void accept(Throwable throwable) throws Exception {
                        messageLabel.set(context.getString(R.string.error_loading_inventory));
                        dolapProgress.set(View.GONE);
                        dolapLabel.set(View.VISIBLE);
                        dolapRecycler.set(View.GONE);
                    }
                });

        compositeDisposable.add(disposable);
    }
    private void changeComponentsDataSet(List<Components> components) {
        componentsList.addAll(components);
        setChanged();
        notifyObservers();
    }
    public List<Components> getComponentsList() {
        return componentsList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
}
