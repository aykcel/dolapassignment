package com.clkasd.dolapassignment.viewModel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.graphics.Paint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.clkasd.dolapassignment.model.Components;
import com.clkasd.dolapassignment.model.ContentTypeEnum;
import com.clkasd.dolapassignment.model.DisplayTypeEnum;
import com.clkasd.dolapassignment.model.Product;
import com.clkasd.dolapassignment.view.ProductsAdapter;

import java.util.List;

/**
 * Created by aykutcelik on 31.01.2018.
 */

public class ItemComponentsViewModel extends BaseObservable {
    private Components component;

    public ObservableInt recyclerVisibility;
    public ObservableInt bannerVisibility;
    public ObservableInt singleBannerVisibility;
    public ObservableInt headerTextVisibility;
    public ObservableField<String> headerText;
    private List<Product> productList;

    public List<Product> getProductList()
    {
        return productList;
    }
    public void setComponent(Components component) {
        this.component = component;
        productList = component.getProductList();
        updateObservables();
        notifyChange();
    }
    public ItemComponentsViewModel(Components component)
    {
        this.component = component;
        productList = component.getProductList();
        recyclerVisibility = new ObservableInt(View.GONE);
        bannerVisibility = new ObservableInt(View.GONE);
        headerTextVisibility = new ObservableInt(View.GONE);
        singleBannerVisibility  = new ObservableInt(View.GONE);
        headerText = new ObservableField<>("");
        updateObservables();
    }
    public String getSingleBannerPicture(){
        if(component.getBannerContentList()!=null && component.getBannerContentList().size()>0)
        {
            return component.getBannerContentList().get(0).getImageUrl();
        }
        else if(component.getHeadlineBannerContentList()!=null
                && component.getContentType()==ContentTypeEnum.PRODUCT
                && component.getDisplayType()== DisplayTypeEnum.SLIDER)
        {
            bannerVisibility.set(View.VISIBLE);
            return component.getHeadlineBannerContentList().getImageUrl();

        }
        return "";
    }
    void updateObservables()
    {
        if(!TextUtils.isEmpty(component.getTitle()))
            headerTextVisibility.set(View.VISIBLE);
        headerText.set(component.getTitle());
        switch (component.getDisplayType())
        {
            case SINGLE:
                recyclerVisibility.set(View.GONE);
                bannerVisibility.set(View.GONE);
                singleBannerVisibility.set(View.VISIBLE);
                break;
            case SLIDER:
                recyclerVisibility.set(View.VISIBLE);
                bannerVisibility.set(View.VISIBLE);
                singleBannerVisibility.set(View.GONE);
                break;
        }
    }
    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }

    @BindingAdapter("strikeThrough") public static void strikeThrough(TextView textView, String makeStrikeThrough) {
        if(makeStrikeThrough.equals("1"))
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
    @BindingAdapter("source") public static void setSource(RecyclerView recyclerView, List<Product> items) {
        if(items!=null && items.size()>0) {
            ProductsAdapter adapter = new ProductsAdapter();
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false));
            adapter.setProductsList(items);
        }
    }
}
