package com.clkasd.dolapassignment.viewModel;

import android.databinding.BaseObservable;
import android.text.TextUtils;

import com.clkasd.dolapassignment.model.Product;

/**
 * Created by aykutcelik on 31.01.2018.
 */

public class ProductsItemViewModel extends BaseObservable {

    private Product product;

    public String getStrikeThrough()
    {
        return "1";
    }
    public String getOriginalPrice()
    {
       return TextUtils.isEmpty(product.getOriginalPrice())?"":product.getOriginalPrice() + " TL";
    }
    public String getPrice()
    {
        return product.getPrice() + " TL";
    }
    public ProductsItemViewModel(Product product)
    {
        this.product = product;
    }

    public String getProductImage()
    {
        return product.getThumbnailImage().getPath();
    }
    public void setProduct(Product product) {
        this.product = product;
    }
}
