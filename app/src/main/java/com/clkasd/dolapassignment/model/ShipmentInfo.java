package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aykut on 29.01.2018.
 */

class ShipmentInfo {
    @SerializedName("shipmentTerm") private String shipmentTerm;
    @SerializedName("shipmentText") private String shipmentText;
}
