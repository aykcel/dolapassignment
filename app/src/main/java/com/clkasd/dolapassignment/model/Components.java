package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aykut on 29.01.2018.
 */

public class Components {
    @SerializedName("id") private int id;
    @SerializedName("title") private String title;
    @SerializedName("contentType") private String contentType;
    @SerializedName("displayType") private String displayType;
    @SerializedName("inventoryKey") private String inventoryKey;
    @SerializedName("bannerContents") private List<BannerContent> bannerContentList;
    @SerializedName("products") private List<Product> productList;
    @SerializedName("headLineBannerContent") private HeadlineBanner headLineBannerContent;
    @SerializedName("displayOrder") private int displayOrder;
    @SerializedName("showInDisplay") private boolean showInDisplay;
    @SerializedName("autoBrand") private boolean autoBrand;
    @SerializedName("autoMember") private boolean autoMember;
    @SerializedName("autoCrossSellRetargeting") private boolean autoCrossSellRetargeting;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ContentTypeEnum getContentType() {
        return ContentTypeEnum.valueOf(contentType);
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public DisplayTypeEnum getDisplayType() {
        return DisplayTypeEnum.valueOf(displayType);
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public String getInventoryKey() {
        return inventoryKey;
    }

    public void setInventoryKey(String inventoryKey) {
        this.inventoryKey = inventoryKey;
    }

    public List<BannerContent> getBannerContentList() {
        return bannerContentList;
    }

    public void setBannerContentList(List<BannerContent> bannerContentList) {
        this.bannerContentList = bannerContentList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public HeadlineBanner getHeadlineBannerContentList() {
        return headLineBannerContent;
    }

    public void setHeadlineBannerContentList(HeadlineBanner headlineBannerContent) {
        this.headLineBannerContent = headlineBannerContent;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public boolean isShowInDisplay() {
        return showInDisplay;
    }

    public void setShowInDisplay(boolean showInDisplay) {
        this.showInDisplay = showInDisplay;
    }

    public boolean isAutoBrand() {
        return autoBrand;
    }

    public void setAutoBrand(boolean autoBrand) {
        this.autoBrand = autoBrand;
    }

    public boolean isAutoMember() {
        return autoMember;
    }

    public void setAutoMember(boolean autoMember) {
        this.autoMember = autoMember;
    }

    public boolean isAutoCrossSellRetargeting() {
        return autoCrossSellRetargeting;
    }

    public void setAutoCrossSellRetargeting(boolean autoCrossSellRetargeting) {
        this.autoCrossSellRetargeting = autoCrossSellRetargeting;
    }


}
