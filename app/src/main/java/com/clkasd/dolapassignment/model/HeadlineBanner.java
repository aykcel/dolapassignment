package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by aykutcelik on 31.01.2018.
 */

public class HeadlineBanner {
    @SerializedName("id") private int id;
    @SerializedName("imageUrl") private String imageUrl;
    @SerializedName("width") private int width;
    @SerializedName("height") private int height;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getDispOrder() {
        return dispOrder;
    }

    public void setDispOrder(int dispOrder) {
        this.dispOrder = dispOrder;
    }

    @SerializedName("colour") private String colour;
    @SerializedName("displayOrder") private int dispOrder;

}
