package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aykut on 29.01.2018.
 */

public class Image {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @SerializedName("id") private int id;
    @SerializedName("path") private String path;
    @SerializedName("width") private int width;
    @SerializedName("height") private int height;
    @SerializedName("colour") private String colour;
}
