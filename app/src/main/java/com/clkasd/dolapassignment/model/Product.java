package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aykut on 29.01.2018.
 */

public class Product {

    @SerializedName("id") private int id;
    @SerializedName("status") private String status;
    @SerializedName("price") private String price;
    @SerializedName("originalPrice") private String originalPrice;
    @SerializedName("condition") private String condition;
    @SerializedName("shipmentTerm") private String shipmentTerm;
    @SerializedName("shipmentInfo") private ShipmentInfo shipmentInfo;
    @SerializedName("description") private String description;
    @SerializedName("likeCount") private Integer likeCount;
    @SerializedName("commentCount") private Integer commentCount;
    @SerializedName("likedByCurrentMember") private Boolean likedByCurrentMember;
    @SerializedName("bidByCurrentMember") private Boolean bidByCurrentMember;
    @SerializedName("allowBidding") private Boolean allowBidding;
    @SerializedName("displayable") private Boolean displayable;
    @SerializedName("fakeControl") private Boolean fakeControl;
    @SerializedName("hasApprovedBid") private Boolean hasApprovedBid;
    @SerializedName("brand") private Brand brand;
    @SerializedName("owner") private Owner owner;
    //@SerializedName("category") private Category category;
    @SerializedName("updatedDate") private String updatedDate;
    @SerializedName("createdDate") private String createdDate;
    //@SerializedName("colours") private List<Object> colours = null;
    @SerializedName("images") private List<Image> images = null;
    @SerializedName("thumbnailImage") private Image thumbnailImage;
    @SerializedName("productQuality") private String productQuality;
    @SerializedName("hiddenTag") private String hiddenTag;
    @SerializedName("originalityVerified") private Boolean originalityVerified;


    public Image getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(Image thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getShipmentTerm() {
        return shipmentTerm;
    }

    public void setShipmentTerm(String shipmentTerm) {
        this.shipmentTerm = shipmentTerm;
    }

    public ShipmentInfo getShipmentInfo() {
        return shipmentInfo;
    }

    public void setShipmentInfo(ShipmentInfo shipmentInfo) {
        this.shipmentInfo = shipmentInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Boolean getLikedByCurrentMember() {
        return likedByCurrentMember;
    }

    public void setLikedByCurrentMember(Boolean likedByCurrentMember) {
        this.likedByCurrentMember = likedByCurrentMember;
    }

    public Boolean getBidByCurrentMember() {
        return bidByCurrentMember;
    }

    public void setBidByCurrentMember(Boolean bidByCurrentMember) {
        this.bidByCurrentMember = bidByCurrentMember;
    }

    public Boolean getAllowBidding() {
        return allowBidding;
    }

    public void setAllowBidding(Boolean allowBidding) {
        this.allowBidding = allowBidding;
    }

    public Boolean getDisplayable() {
        return displayable;
    }

    public void setDisplayable(Boolean displayable) {
        this.displayable = displayable;
    }

    public Boolean getFakeControl() {
        return fakeControl;
    }

    public void setFakeControl(Boolean fakeControl) {
        this.fakeControl = fakeControl;
    }

    public Boolean getHasApprovedBid() {
        return hasApprovedBid;
    }

    public void setHasApprovedBid(Boolean hasApprovedBid) {
        this.hasApprovedBid = hasApprovedBid;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getProductQuality() {
        return productQuality;
    }

    public void setProductQuality(String productQuality) {
        this.productQuality = productQuality;
    }

    public String getHiddenTag() {
        return hiddenTag;
    }

    public void setHiddenTag(String hiddenTag) {
        this.hiddenTag = hiddenTag;
    }

    public Boolean getOriginalityVerified() {
        return originalityVerified;
    }

    public void setOriginalityVerified(Boolean originalityVerified) {
        this.originalityVerified = originalityVerified;
    }


}
