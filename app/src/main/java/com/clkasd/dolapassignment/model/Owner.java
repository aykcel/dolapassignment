package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aykut on 29.01.2018.
 */

public class Owner {
    @SerializedName("id") private int id;
    @SerializedName("image") private Image image;
    @SerializedName("coverImage") private Image coverImage;
    @SerializedName("nickname") private String nickname;
    @SerializedName("bioText") private String bioText;
    @SerializedName("active") private boolean active;
    //@SerializedName("sneakPeekProducts") private List<Object> sneakPeekProducts = null;
    @SerializedName("lastActiveDate") private String lastActiveDate;
    //@SerializedName("feedback") private Feedback feedback;
    @SerializedName("vacationMode") private Boolean vacationMode;
    @SerializedName("ambassadorLevel") private String ambassadorLevel;
    @SerializedName("followedByCurrentMember") private Boolean followedByCurrentMember;
    @SerializedName("productAndFollowCount") private Integer productAndFollowCount;
}
