package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aykut on 29.01.2018.
 */

class Brand {
    @SerializedName("id") private int id;
    @SerializedName("title") private String title;
    @SerializedName("forKidsBaby") private boolean forKidsBaby;
    @SerializedName("forWoman") private boolean forWoman;
    @SerializedName("brandType") private String brandType;
}
