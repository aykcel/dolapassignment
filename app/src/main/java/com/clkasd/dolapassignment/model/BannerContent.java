package com.clkasd.dolapassignment.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aykut on 29.01.2018.
 */

public class BannerContent {


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }
    @SerializedName("id") private int id;
    @SerializedName("imageUrl") private String imageUrl;
    @SerializedName("width") private int width;
    @SerializedName("height") private int height;
    @SerializedName("displayOrder") private int displayOrder;
}
