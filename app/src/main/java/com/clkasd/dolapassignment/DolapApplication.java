package com.clkasd.dolapassignment;

import android.app.Application;
import android.content.Context;

import com.clkasd.dolapassignment.data.DolapFactory;
import com.clkasd.dolapassignment.data.DolapService;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Aykut on 29.01.2018.
 */

public class DolapApplication extends Application {

    private DolapService dolapService;
    private Scheduler scheduler;

    private static DolapApplication get(Context context) {
        return (DolapApplication) context.getApplicationContext();
    }

    public static DolapApplication create(Context context) {
        return DolapApplication.get(context);
    }

    public DolapService getDolapService() {
        if (dolapService == null) {
            dolapService = DolapFactory.create();
        }

        return dolapService;
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }

    public void setDolapService(DolapService dolapService) {
        this.dolapService = dolapService;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
}

