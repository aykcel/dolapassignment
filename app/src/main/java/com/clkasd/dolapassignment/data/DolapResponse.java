package com.clkasd.dolapassignment.data;

import com.clkasd.dolapassignment.model.Components;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aykut on 29.01.2018.
 */

public class DolapResponse {
    @SerializedName("id") private int id;
    @SerializedName("name") private String name;
    @SerializedName("components") private List<Components> componentsList;

    public List<Components> getComponentsList()
    {
        return componentsList;
    }
}
