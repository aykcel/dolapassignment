package com.clkasd.dolapassignment.data;

import com.clkasd.dolapassignment.model.Image;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Aykut on 29.01.2018.
 */

public interface DolapService {
    @Headers({
            ("Content-Type: application/json"),
            ("AppVersion: 70"),
            ("AppPlatform: android")
    })
    @POST("/inventory/display/interview/")
    Observable<DolapResponse> fetchResponse(@Body Image body);
}
