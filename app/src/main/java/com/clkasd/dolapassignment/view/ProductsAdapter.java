package com.clkasd.dolapassignment.view;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.clkasd.dolapassignment.model.Product;
import com.clkasd.dolapassignment.R;
import com.clkasd.dolapassignment.viewModel.ProductsItemViewModel;
import com.clkasd.dolapassignment.databinding.ItemProductsBinding;
import java.util.Collections;
import java.util.List;

/**
 * Created by aykutcelik on 31.01.2018.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductAdapterViewHolder>{

    private List<Product> productsList;

    public ProductsAdapter()
    {
        productsList = Collections.emptyList();
    }
    @Override
    public ProductsAdapter.ProductAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProductsBinding itemProductsBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_products, parent, false);
        return new ProductsAdapter.ProductAdapterViewHolder(itemProductsBinding);
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.ProductAdapterViewHolder holder, int position) {
        holder.bindProducts(productsList.get(position));
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }

    public void setProductsList(List<Product> productsList) {
        this.productsList = productsList;
        notifyDataSetChanged();
    }
    public static class ProductAdapterViewHolder extends RecyclerView.ViewHolder {
        ItemProductsBinding itemProductsBinding;
        public ProductAdapterViewHolder(ItemProductsBinding itemProductsBinding) {
            super(itemProductsBinding.itemProductLinlay);
            this.itemProductsBinding = itemProductsBinding;
        }
        void bindProducts(Product product)
        {
            if (itemProductsBinding.getItemProductViewModel() == null) {
                itemProductsBinding.setItemProductViewModel(
                        new ProductsItemViewModel(product));
            } else {
                itemProductsBinding.getItemProductViewModel().setProduct(product);
            }
        }
    }
}
