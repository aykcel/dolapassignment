package com.clkasd.dolapassignment.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.clkasd.dolapassignment.R;
import com.clkasd.dolapassignment.viewModel.DashboardViewModel;
import com.clkasd.dolapassignment.databinding.ActivityDashboardBinding;

import java.util.Observable;
import java.util.Observer;

public class DashboardActivity extends AppCompatActivity implements Observer {

    ActivityDashboardBinding activityDashboardBinding;
    DashboardViewModel dashboardViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initDataBinding();
        setSupportActionBar(activityDashboardBinding.toolbar);
        setupListDolapView(activityDashboardBinding.listComponents);
        setupObserver(dashboardViewModel);
    }

    private void initDataBinding() {
        activityDashboardBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        dashboardViewModel = new DashboardViewModel(this);
        activityDashboardBinding.setMainViewModel(dashboardViewModel);
    }
    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof DashboardViewModel) {
            DolapAdapter dolapAdapter = (DolapAdapter) activityDashboardBinding.listComponents.getAdapter();
            DashboardViewModel dolapViewModel = (DashboardViewModel) observable;
            dolapAdapter.setComponentsList(dolapViewModel.getComponentsList());
        }
    }

    private void setupListDolapView(RecyclerView listPeople) {
        DolapAdapter adapter = new DolapAdapter();
        listPeople.setAdapter(adapter);
        listPeople.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dashboardViewModel.reset();
    }
}
