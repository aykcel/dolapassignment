package com.clkasd.dolapassignment.view;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.clkasd.dolapassignment.model.Components;
import com.clkasd.dolapassignment.R;
import com.clkasd.dolapassignment.viewModel.ItemComponentsViewModel;
import com.clkasd.dolapassignment.databinding.ItemComponentBinding;

import java.util.Collections;
import java.util.List;

/**
 * Created by Aykut on 31.01.2018.
 */

public class DolapAdapter extends RecyclerView.Adapter<DolapAdapter.ComponentAdapterViewHolder>{

    private List<Components> componentsList;

    public DolapAdapter()
    {
        componentsList = Collections.emptyList();
    }
    @Override
    public ComponentAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemComponentBinding itemComponentBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_component, parent, false);
        return new ComponentAdapterViewHolder(itemComponentBinding);
    }

    @Override
    public void onBindViewHolder(ComponentAdapterViewHolder holder, int position) {
        holder.bindComponents(componentsList.get(position));
    }

    @Override
    public int getItemCount() {
        return componentsList.size();
    }

    public void setComponentsList(List<Components> componentsList) {
        this.componentsList = componentsList;
        notifyDataSetChanged();
    }
    public static class ComponentAdapterViewHolder extends RecyclerView.ViewHolder {
        ItemComponentBinding itemComponentBinding;
        public ComponentAdapterViewHolder(ItemComponentBinding itemComponentBinding) {
            super(itemComponentBinding.itemComponentRellay);
            this.itemComponentBinding = itemComponentBinding;
        }
        void bindComponents(Components component)
        {
            if (itemComponentBinding.getItemComponentViewModel() == null) {
                itemComponentBinding.setItemComponentViewModel(
                        new ItemComponentsViewModel(component));
            } else {
                itemComponentBinding.getItemComponentViewModel().setComponent(component);
            }
        }
    }
}
